country_decisions = {
	dredge_the_canals = {
		major = yes
	
		potential = {
			owns = 601
			601 = {
				has_province_modifier = old_bulwari_canals
				NOT = { has_construction = building }
			}
		}
		
		provinces_to_highlight = {
		    province_id = 601
		}	
		
		allow = {
			treasury = 500
			is_at_war = no
		}
	
		effect = {
			601 = {
				add_building_construction = {
					building = bulwari_canals
					speed = 1
					cost = 1
				}
			}
		}
		
	}
	
	repair_locks_and_lifts = {
		major = yes
	
		potential = {
			owns = 601
			601 = {
				has_province_modifier = bulwari_canals
				NOT = { has_construction = building }
			}
		}
		
		provinces_to_highlight = {
		    province_id = 601
		}	
		
		allow = {
			treasury = 1000
			is_at_war = no
		}
	
		effect = {
			601 = {
				add_building_construction = {
					building = bulwari_locks_and_lifts
					speed = 1
					cost = 1
				}
			}
		}
		
	}
	
	
	build_great_bulwari_watercourse = {
		major = yes
	
		potential = {
			owns = 601
			601 = {
				has_province_modifier = bulwari_locks_and_lifts
				NOT = { has_construction = building }
			}
		}
		
		provinces_to_highlight = {
		    province_id = 601
		}	
		
		allow = {
			treasury = 1000
			is_at_war = no
		}
	
		effect = {
			601 = {
				add_building_construction = {
					building = great_bulwari_watercourse
					speed = 1
					cost = 1
				}
			}
		}
		
	}
}